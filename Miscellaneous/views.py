from django.shortcuts import render
from django.db.models import Q
from Accounts import models as AccountModels
from Projects import models as ProjectModels

def searchbar(request):
    template = 'Miscellaneous/search.html'
    return render(request, template)


def search(request):
    template = 'Miscellaneous/results.html'
    query = request.GET.get('q')
    results = POST.objects.filter(Q(title__icontains=query))
    pages = pagination(request, results, num=1)
    context = {
        'items': pages[0],
        'page_range': pages[1] 
    }
    return render(request, template, context)
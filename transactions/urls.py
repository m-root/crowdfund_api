from django.urls import path
from . import views

urlpatterns = [

    path('set_amount/', views.PreTransactionView.as_view(), name='transact'),
    path('submit_amount/', views.amountform, name='amount_form'),
    path('pay/', views.TransactionView, name='pay')
    
]

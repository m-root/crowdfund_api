function validateForm() {
    var address = document.forms["registrationForm"]["address"].value;
    var IFSCCode = document.forms["registrationForm"]["IFSCCode"].value;
    var accountNumber = document.forms["registrationForm"]["accountNumber"].value;
    var reEnterAccountNumber = document.forms["registrationForm"]["reEnterAccountNumber"].value;
    var beneficiaryName = document.forms["registrationForm"]["beneficiaryName"].value;
    var PANNumber = document.forms["registrationForm"]["PANNumber"].value;

    if (address == "" || IFSCCode == '' || accountNumber == '' || reEnterAccountNumber == '' || beneficiaryName == '' || PANNumber == '') {
        alert("All field must be filled out");
    } else {

        document.getElementById("tab-profile2").style.display = "block";
        document.getElementById("tab-home2").style.display = "none";

        document.getElementById("registrationTabButton").classList.remove("active");
        document.getElementById("docsUploadTabButton").classList.add("active");

    }
}



function showDocsPage() {
    document.getElementById("tab-profile2").style.display = "block";
    document.getElementById("tab-home2").style.display = "none";

    document.getElementById("registrationTabButton").classList.remove("active");
    document.getElementById("docsUploadTabButton").classList.add("active");


    // alert('fff')
}

function showRegistrationPage() {
    document.getElementById("registrationTabButton").classList.add("active");
    document.getElementById("docsUploadTabButton").classList.remove("active");

    document.getElementById("tab-profile2").style.display = "none";
    document.getElementById("tab-home2").style.display = "block";
}





function projectRegistrationValidateForm() {
    var projectTitle = document.forms["projectForm"]["projectTitle"].value;
    var abstract = document.forms["projectForm"]["abstract"].value;
    var description = document.forms["projectForm"]["description"].value;
    var projectSignificance = document.forms["projectForm"]["projectSignificance"].value;

    if (projectTitle == "" || abstract == '' || description == '' || projectSignificance == '') {
        alert("All field must be filled out");
    } else {
        document.getElementById("tab-profile3").style.display = "block";
        document.getElementById("tab-home3").style.display = "none";
        document.getElementById("tab-project3").style.display = "none";

        document.getElementById("createProjectPage1Tab").classList.remove("active");
        document.getElementById("createProjectPage3Tab").classList.remove("active");
        document.getElementById("createProjectPage2Tab").classList.add("active");
    }

}







function showCreateProjectPage2() {
    document.getElementById("tab-profile3").style.display = "block";
    document.getElementById("tab-home3").style.display = "none";
    document.getElementById("tab-project3").style.display = "none";

    document.getElementById("createProjectPage1Tab").classList.remove("active");
    document.getElementById("createProjectPage3Tab").classList.remove("active");
    document.getElementById("createProjectPage2Tab").classList.add("active");
}

function showCreateProjectPage3() {
    document.getElementById("tab-project3").style.display = "block";
    document.getElementById("tab-profile3").style.display = "none";
    document.getElementById("tab-home3").style.display = "none";

    document.getElementById("createProjectPage1Tab").classList.remove("active");
    document.getElementById("createProjectPage2Tab").classList.remove("active");
    document.getElementById("createProjectPage3Tab").classList.add("active");
}


function showCreateProjectPage1() {
    document.getElementById("tab-profile3").style.display = "none";
    document.getElementById("tab-home3").style.display = "block";
    document.getElementById("tab-project3").style.display = "none";
    document.getElementById("createProjectPage1Tab").classList.remove("active");
    document.getElementById("createProjectPage1Tab").classList.add("active");
    document.getElementById("createProjectPage2Tab").classList.remove("active");
}




function showNewProjectPage3(){
    var projectTitle = document.forms["projectFormAdditionalDetails"]["projectTitle"].value;
    var abstract = document.forms["projectFormAdditionalDetails"]["abstract"].value;
    var description = document.forms["projectFormAdditionalDetails"]["description"].value;
    var projectSignificance = document.forms["projectFormAdditionalDetails"]["projectSignificance"].value;

    if (projectTitle == "" || abstract == '' || description == '' || projectSignificance == '') {
        alert("All field must be filled out");
    } else {

        document.getElementById("tab-profile3").style.display = "block";
        document.getElementById("tab-home3").style.display = "none";

        document.getElementById("tab-project3").style.display = "none";

        document.getElementById("createProjectPage1Tab").classList.remove("active");
        document.getElementById("createProjectPage3Tab").classList.remove("active");

        document.getElementById("createProjectPage2Tab").classList.add("active");

    }
}

function projectRegistrationFormValidation(){
    var projectTitle = document.forms["projectForm"]["projectTitle"].value;
    var abstract = document.forms["projectForm"]["abstract"].value;
    var description = document.forms["projectForm"]["description"].value;
    var projectSignificance = document.forms["projectForm"]["projectSignificance"].value;

    var references = document.forms["projectFormPart2"]["references"].value;

    var projectFormAdditionalDetails = document.forms["projectFormPart2"]["projectFormAdditionalDetails"].value;

    var deadline= document.forms["projectFormPart3"]["deadline"].value;
    var targetAmount = document.forms["projectFormPart3"]["targetAmount"].value;

    if (projectTitle == "" || abstract == '' || description == '' || projectSignificance == '' || references == '' || projectFormAdditionalDetails == '' || targetAmount == '' || deadline == '') {
        alert("All field must be filled out");
    }
    
    else{
        document.getElementById("projectRegistrationFormSubmission").dataType = "success";
        document.getElementById("modal--default").style.display = "none";
        document.getElementById('projectRegistrationFormSubmission'.dataType = 'danger')
    }


}


// // function hideDocsPage() {
// // var x = document.getElementById("tab-profile-2");
// // x.classList.remove("active");
// // alert(x.classList);

// // document.getElementById("docsUploadContainer").style.display = "none";

// //     var z = document.getElementById("tab-profile-2");
// //     alert(z.classList);
// //     z.classList.remove("active")
// //     z.classList.remove("in")
// // }
// // document.getElementById("registrationTabLink").onclick = function () {};

// //   || IFSCCode == '' || accountNumber == '' || reEnterAccountNumber == '' || beneficiaryName == '' || PANNumber == ''
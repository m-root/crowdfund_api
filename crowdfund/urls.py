from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include, re_path

from Accounts import views as account_views
from Projects import views as project_views
from Miscellaneous import views as MiscViews




urlpatterns = [
    path('admin/', admin.site.urls),
#  path('', account_views.LandingPageView.as_view(), name='home'),
    path('', account_views.DashboardView.as_view(), name='home'),
    path('user_accounts/', include('Accounts.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/', include('allauth.urls')),
    path('transactions/', include('transactions.urls')),
    path('projects/', include('Projects.urls')),
    path('<str:username>/<str:slug>/', project_views.ProjectDetailView, name='project_details'),
    path('dashboard/', account_views.DashboardView.as_view(), name='dashboard'),
    path('login/', account_views.LoginFormDef, name='login'),
    path('logout/', account_views.LogoutView, name='logout'),

    path('signup/', account_views.SignupView.as_view(), name='signup'),
    path('signupform/', account_views.signup_form, name='signupform'),
    re_path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            account_views.activate, name='activate'),
    path('browse/', project_views.browse_projects, name='browse_projects'),
    path('misc/', include('Miscellaneous.urls')),

    path('<str:username>/', account_views.UserProfileView, name='user_profile'),


]


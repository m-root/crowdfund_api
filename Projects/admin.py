from django.contrib import admin
from . import models

admin.site.register(models.ProjectDetails)
admin.site.register(models.References)
admin.site.register(models.BankInformation)
admin.site.register(models.ProjectTags)
admin.site.register(models.Collaborators)
admin.site.register(models.Contributors)
admin.site.register(models.Recommendations)
admin.site.register(models.Bookmarks)


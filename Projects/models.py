from django.db import models
from django.conf import settings


# Create your models here.


class ProjectDetails(models.Model):
    project_id = models.AutoField(primary_key=True)
    
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.CASCADE)
    title = models.CharField(max_length=100)
    project_slug = models.CharField(max_length=50)
    abstract = models.TextField()
    description = models.TextField()
    significance = models.TextField()
    additional_details = models.TextField(blank=True)
    target_amount = models.IntegerField(null=True, default=1000000)
    balance_amount = models.IntegerField(null=True, default=0)
    completion_percentage = models.FloatField(default=0, blank=True)
    reference1 = models.CharField(max_length=100, blank=True)
    reference2 = models.CharField(max_length=100, blank=True)
    reference3 = models.CharField(max_length=100, blank=True)
    codebase = models.CharField(max_length=50, blank=True)
    dataset = models.CharField(max_length=50, blank=True)
    intellectual_property = models.CharField(max_length=50, blank=True)
    accept_collaborators = models.CharField(max_length=3)
    accept_interns = models.CharField(max_length=3)
    accept_mentors = models.CharField(max_length=3)
    affiliation = models.CharField(max_length=50, default="Independent Research")
    fees = models.IntegerField(default=7)
    # deadline = models.DateField(blank=True, null=True)
    new_dead = models.DateField(blank=True, null=True, default=None)

    def __str__(self):
        return ("{}".format(self.title))

class References(models.Model):
    project_id = models.ForeignKey(ProjectDetails, on_delete=models.CASCADE)
    link_url = models.CharField(max_length=100, null=True, blank=True)

class BankInformation(models.Model):
    project_id = models.OneToOneField(ProjectDetails, on_delete=models.CASCADE)
    bank_ifsc = models.CharField(max_length=11)
    account_no = models.CharField(max_length=18)
    beneficiary_name = models.CharField(max_length=50)

class ProjectTags(models.Model):
    project_id = models.ForeignKey(ProjectDetails, on_delete=models.CASCADE, related_name='tags')
    tag = models.CharField(max_length=50)




class Contributors(models.Model):
    project_id = models.ForeignKey(ProjectDetails, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    amount = models.IntegerField()

class Recommendations(models.Model):
    recom_id = models.AutoField(primary_key=True)
    project_id = models.ForeignKey(ProjectDetails, on_delete=models.CASCADE)
    writing_user =  models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    Text = models.CharField(max_length=10000)
    date_written = models.DateTimeField(auto_now=True, editable=False)
    approved = models.BooleanField(default=False)

class Bookmarks(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    project_id = models.ForeignKey(ProjectDetails, on_delete=models.CASCADE)

class Mentors(models.Model):
    mentor_id = models.AutoField(primary_key=True)
    project_id = models.ForeignKey(ProjectDetails, on_delete=models.CASCADE)
    requesting_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    body = models.CharField(max_length=10000)
    date_requested = models.DateTimeField(auto_now=True, editable=False)
    approved = models.BooleanField(default=False)

class Interns(models.Model):
    intern_id = models.AutoField(primary_key=True)
    project_id = models.ForeignKey(ProjectDetails, on_delete=models.CASCADE)
    requesting_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    body = models.CharField(max_length=10000)
    date_requested = models.DateTimeField(auto_now=True, editable=False)
    approved = models.BooleanField(default=False)


class Collaborators(models.Model):
    collaborator_id = models.AutoField(primary_key=True)
    project_id = models.ForeignKey(ProjectDetails, on_delete=models.CASCADE, related_name='collaborators')
    requesting_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    body = models.CharField(max_length=10000)
    date_requested = models.DateTimeField(auto_now=True, editable=False)
    approved = models.BooleanField(default=False)
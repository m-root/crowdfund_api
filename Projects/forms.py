from django import forms
from django.core.exceptions import ValidationError

from . import models



class AddProjectForm(forms.Form):
    title = forms.CharField(max_length=100)
    project_slug = forms.CharField(max_length=50)
    abstract = forms.CharField(max_length=1000)

    description = forms.CharField(max_length=1000)
    significance = forms.CharField(max_length=1000)
    additional_details = forms.CharField(max_length=1000, required=False)
    collaborators = forms.CharField(max_length=100, required=False)
    reference1 = forms.CharField(max_length=50, required=False)
    reference2 = forms.CharField(max_length=50, required=False)
    reference3 = forms.CharField(max_length=50, required=False)
    target_amount = forms.IntegerField(required=False)
    tags = forms.CharField(max_length=1000, required=False)
    codebase = forms.CharField(max_length=50, required=False)
    dataset = forms.CharField(max_length=50, required=False)
    intellectual_property = forms.CharField(max_length=50, required=False)
    accept_collaborators = forms.CharField(max_length=4, required=False)
    accept_interns = forms.CharField(max_length=4, required=False)
    accept_mentors = forms.CharField(max_length=4, required=False)
    # deadline_day = forms.CharField(max_length=3)
    # deadline_month = forms.CharField(max_length=3)
    # deadline_year = forms.CharField(max_length=4)
    affiliation = forms.CharField(max_length=50, required=False)
    T_and_C = forms.CharField(max_length=5)


    def clean(self):
        data = super().clean()
        ref1 = data.get('reference1')
        ref2 = data.get('reference2')
        ref3 = data.get('reference3')
        references = [ref1,ref2,ref3]
        if ref1!='' and ref2!='' and ref3!='':
            if len(set(references)) != len(references):
                raise ValidationError('All The References Must Be Distinct')

        return data

class RecommendationForm(forms.Form):
    Text = forms.CharField(max_length=10000)

class EditProjectForm(forms.Form):
    title = forms.CharField(max_length=100, required=False)
    abstract = forms.CharField(max_length=1000, required=False)
    description = forms.CharField(max_length=1000, required=False)
    significance = forms.CharField(max_length=1000, required=False)
    additional_details = forms.CharField(max_length=1000, required=False)
    collaborators = forms.CharField(max_length=100, required=False)
    reference1 = forms.CharField(max_length=50, required=False)
    reference2 = forms.CharField(max_length=50, required=False)
    reference3 = forms.CharField(max_length=50, required=False)
    tags = forms.CharField(max_length=1000, required=False)
    codebase = forms.CharField(max_length=50, required=False)
    dataset = forms.CharField(max_length=50, required=False)
    intellectual_property = forms.CharField(max_length=50, required=False)
    accept_collaborators = forms.CharField(max_length=4, required=False)
    accept_interns = forms.CharField(max_length=4, required=False)
    accept_mentors = forms.CharField(max_length=4, required=False)
    # deadline_day = forms.CharField(max_length=3)
    # deadline_month = forms.CharField(max_length=3)
    # deadline_year = forms.CharField(max_length=4)
    affiliation = forms.CharField(max_length=50, required=False)
    # T_and_C = forms.CharField(max_length=5)
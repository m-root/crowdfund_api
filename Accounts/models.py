from django.db import models
from django.contrib.auth.models import AbstractUser
from Projects.models import ProjectDetails


class CustomUser(AbstractUser):
    email = models.EmailField(unique=True)
    kyc_done = models.BooleanField(default=False)
    kyc_applied = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now=True, auto_now_add=False)
    user_accept_collaborations = models.BooleanField(default=False)
    user_accept_interns = models.BooleanField(default=False)


class Profile(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    date_of_birth = models.DateField(null=True, blank=True)
    institute = models.CharField(max_length=30, null=True)
    bio = models.TextField(null=True, blank=True)
    website = models.URLField(null=True, blank=True)
    phone_no = models.IntegerField(null=True, blank=True, unique=True)
    updated = models.DateTimeField(auto_now_add=True)
    designation = models.CharField(max_length=100, blank=True, null=True)
    linkedin_handle = models.CharField(max_length=50, blank=True, null=True)
    instagram_handle = models.CharField(max_length=50, blank=True, null=True)
    twitter_handle = models.CharField(max_length=50, blank=True, null=True)
    codebase_handle = models.CharField(max_length=50, blank=True, null=True)



class BankDetails(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    bank_name = models.CharField(max_length=100, blank=True)
    account_no = models.CharField(max_length=18, blank=True)
    ifsc = models.CharField(max_length=11, blank=True)
    beneficiary = models.CharField(max_length=50, blank=True)
    pan = models.CharField(max_length=10, blank=True)

class History(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    time_of_action = models.DateTimeField(auto_now=True)
    type_of_action = models.CharField(max_length=50)
    project_id = models.ForeignKey(ProjectDetails, null=True, on_delete=models.CASCADE)

class Subsriptions(models.Model):
    Subscriber = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='subscriber')
    Subscribed_To = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='subscribed_to')

from django.urls import path, re_path
from . import views as account_views
from Projects import views as project_views
from Miscellaneous import views as MiscViews

app_name = "Accounts"

urlpatterns = [
    # Registeration URLs:
    # path('signup/', account_views.SignupView.as_view(), name='signup'),
    # path('signupform/', account_views.signup_form, name='signupform'),
    re_path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        account_views.activate, name='activate'),

    # Login URLs:
    # path('login/', account_views.LoginFormDef, name='login'),
    # path('logout/', account_views.LogoutView, name='logout'),

    # Dashboard URLs:
    # path('dashboard/', account_views.DashboardView.as_view(), name='dashboard'),
    path('about/', account_views.ProfileAboutView.as_view(), name='profile_about'),
    path('pricing/', account_views.PricingView.as_view(), name='pricing'),

    # Profile URLs:
    path('account_settings/', account_views.AccountSettingsView.as_view(), name='account_settings'),
    path('profile_change/', account_views.profile_change_form, name='profile_change'),
    path('account_info_change/', account_views.account_info_change_form, name='account_info_change'),
    path('bank_details_change/', account_views.bank_details_change_form, name = 'bank_details_change'),

    # Feed URLs:
    path('subscribe/<str:viewed_user>', account_views.subscribe, name='subscribe'),
    path('unsubscribe/<str:viewed_user>', account_views.unsubscribe, name='unsubscribe'),

    # path('user_internship/<str:username>/', account_views.user_internship_form, name='user_internship_form'),
    # path('user_collaboration/<str:username>/', account_views.user_collaboration_form, name='user_collaboration_form'),



]
